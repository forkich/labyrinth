# Labyrinth
A video game version of the [Labyrinth](https://en.wikipedia.org/wiki/Labyrinth_(paper-and-pencil_game)) paper-and-pencil game
## Launching instructions
To launch the game, run the labyrinth-gui.py script located in the labyrinth directory.  
Specify the --config option to use an existing configuration file or leave it out to specify  
the jid, the chatroom and the password, as well as the game settings at runtime and  
create a new configuration file.

The configuration file stores:
* the jid and the chatroom
* the game settings (the rules, the bindings, the layout and the log file)
