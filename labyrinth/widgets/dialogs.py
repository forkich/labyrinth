# dialogs.py

from tkinter import ttk, filedialog
import tkinter as tk

import sys
import os
import json

from modules.jabberproxy import JabberProxy
from modules.gameinfo import GameInfo

GRID_DEFAULTS = dict(sticky="news", ipadx=5)

class JabberConnectionDialog(tk.Toplevel):

    def __init__(self, root, **kwargs):
        self.__root = root
        tk.Toplevel.__init__(self, root, **kwargs)

        self.minsize(300, 100)
        self.title(GameInfo.config.XMPP.jid)
        self.transient(self.__root)
        self.protocol("WM_DELETE_WINDOW", self.__on_close)

        self.__labels = {}
        self.__entries = {}
        row = 0

        for k in ["jid", "password", "chat"]:
            show = None
            if k == "password":
                v = GameInfo.config.password
                show = '*'
            else:
                v = GameInfo.config.parser["XMPP"][k]

            self.__labels[k] = tk.Label(self, text=k, anchor=tk.W)
            self.__labels[k].grid(row=row, column=0, **GRID_DEFAULTS)

            self.__entries[k] = tk.Entry(self, width=50, show=show)
            if v:
                self.__entries[k].insert(0, v)
            self.__entries[k].grid(row=row, column=1, **GRID_DEFAULTS)

            row += 1

        self.__btn_connect = tk.Button(
            self,
            text="Connect",
            command=self.__connect)
        self.__btn_connect.grid(row=row, column=0, columnspan=2, **GRID_DEFAULTS)

    def __connect(self):

        GameInfo.config.XMPP.jid = self.__entries["jid"].get()
        GameInfo.config.XMPP.chat = self.__entries["chat"].get()
        GameInfo.config.password = self.__entries["password"].get()

        if not GameInfo.config.path:
            GameInfo.config.path = filedialog.asksaveasfilename(
                title="Config file",
                initialdir=os.path.dirname('.'),
                filetypes=[("ini", "*.ini")],
                defaultextension=".ini"
            )

        GameInfo.config.save()
        GameInfo.connect()
        self.__on_close()

    def __on_close(self):
        self.destroy()

class NewGameDialog(tk.Toplevel):

    CONFIG_SECTION = "GameMaster"
    FILE_TYPES = {
        "layout"   : [("layout",   "*.txt")],
        "rules"    : [("rules",    "*.py")],
        "bindings" : [("bindings", "*.py")],
        "log"      : [("log",      "*.log")]
    }

    def __init__(self, root, **kwargs):
        self.__root = root
        tk.Toplevel.__init__(self, root, **kwargs)

        self.minsize(300, 100)
        self.title("New Game")
        self.transient(self.__root)
        self.protocol("WM_DELETE_WINDOW", self.__on_close)

        self.__buttons = {}
        self.__entries = {}
        row = 0

        if self.CONFIG_SECTION not in GameInfo.config.parser:
            template = { k : "" for k in self.FILE_TYPES.keys() }
            GameInfo.config.parser.read_dict({
                self.CONFIG_SECTION : template
            })

        for k in self.FILE_TYPES.keys():

            v = GameInfo.config.parser[self.CONFIG_SECTION][k]

            self.__buttons[k] = tk.Button(
                self,
                text=k,
                anchor=tk.W,
                command=lambda entry=k: self.__open_file(entry))
            self.__buttons[k].grid(row=row, column=0, **GRID_DEFAULTS)

            self.__entries[k] = tk.Entry(self, width=50)
            self.__entries[k].insert(0, v)
            self.__entries[k].grid(row=row, column=1, **GRID_DEFAULTS)

            row += 1

        self.__btn_create = tk.Button(
            self,
            text="Create Game",
            command=self.__create)
        self.__btn_create.grid(row=row, column=0, columnspan=2, **GRID_DEFAULTS)

    def __create(self):
        cfg = GameInfo.config.parser[self.CONFIG_SECTION]
        for item in self.FILE_TYPES.keys():
            cfg[item] = self.__entries[item].get()
        GameInfo.config.save()

        # TODO: highlight mandatory fields if not present
        GameInfo.create_game()

        self.__on_close()

    def __open_file(self, entry):
        path = self.__entries[entry].get()
        if entry == "log":
            path = filedialog.asksaveasfilename(
                title="Log file",
                initialdir=os.path.dirname(path if path else '.'),
                filetypes=self.FILE_TYPES[entry],
                defaultextension=".log"
            )
        else:
            path = filedialog.askopenfilename(
                title=entry,
                initialdir=os.path.dirname(path if path else '.'),
                filetypes=self.FILE_TYPES[entry]
            )
        if path:
            self.__entries[entry].delete(0, tk.END)
            self.__entries[entry].insert(0, os.path.relpath(path))

    def __on_close(self):
        self.destroy()

class AddPlayerDialog(tk.Toplevel):

    def __init__(self, root, **kwargs):
        self.__root = root
        tk.Toplevel.__init__(self, root, **kwargs)

        self.__player_name = tk.StringVar()
        self.__player_color = tk.StringVar()

        self.geometry("+%d+%d" % (root.winfo_x(), root.winfo_y()))
        self.title("Add Player")

        colors = ["#ff0000", "#ffff00", "#00ff00", "#00ffff", "#0000ff", "#ff00ff"]

        ttk.Label(self, text="Name").grid(row=0, column=0, sticky="news")
        player_name = ttk.Entry(self, textvariable=self.__player_name)
        player_name.grid(row=0, column=1, columnspan=len(colors) - 1, sticky="news")
        player_name.bind('<Return>', self.__apply)
        player_name.focus_set()

#        players = Config.get_instance().players
#        lookup = dict(zip(players.values(), players.keys()))

        for ci in range(len(colors)):
#            label, state = (lookup[colors[ci]], tk.DISABLED) if colors[ci] in lookup else (colors[ci], tk.NORMAL)
            label = colors[ci]
            state = tk.NORMAL
            btn_player_color = tk.Radiobutton(self,
                bg="black",
                text=label,
                state=state,
                fg=colors[ci],
                activebackground=colors[ci],
                disabledforeground=colors[ci],
                selectcolor=colors[ci],
                value=colors[ci],
                variable=self.__player_color,
                indicatoron=False,
                takefocus=False,
                width=9,
                height=2)
            btn_player_color.grid(row=1, column=ci)
            if not self.__player_color.get() and state == tk.NORMAL:
                btn_player_color.select()

        btn_create = ttk.Button(self, text="Create", command=self.__apply)
        btn_create.grid(row=2,
                        column=0,
                        sticky="news",
                        columnspan=len(colors))

#        self.__x = map_x // parent.font.measure("+###")
#        self.__y = map_y // (2 * parent.font.metrics("linespace"))

    def __apply(self, src=None):
        msg = json.dumps(
            dict(
                topic="Game.enter",
                nick=self.__player_name.get(),
                color=self.__player_color.get()
            )
        )
        # TODO: implement drop-down list to select game master
        JabberProxy.send(msg, "dendvz@jabber.ru")
#        GameInfo.publish(
#            "Game.enter",
#            nick=self.__player_name.get(),
#            color=self.__player_color.get()
#        )
        self.destroy()

