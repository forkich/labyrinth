import typing

from player import Player
from objects import ObjBase

class RiverBase(ObjBase):
    pass

class River(RiverBase):

    class Node(typing.NamedTuple):
        symbol: str
        next_pos: tuple = None

    def update_status(self, player, end=False):
        if not end:
            status = {"Player carried by the river": player.name}
        else:
            status = {"Player at the end of the river": player.name}
        self.update('', **status)

    def __init__(self, nodes_dict=None):
        if nodes_dict is None:
            nodes_dict = {}
        self.__nodes_dict = nodes_dict

    def add_node(self, node, pos):
        self.__nodes_dict[pos] = node

    def __contains__(self, item):
        return item in self.__nodes_dict

    def teleport_player(self, player: Player):
        try:
            next_pos = self.__nodes_dict[player.pos].next_pos
            if next_pos:
                if self.__nodes_dict[next_pos].next_pos:
                    next_pos = self.__nodes_dict[next_pos].next_pos
                player.move_abs(*next_pos)
                self.update_status(player)
            else:
                self.update_status(player, True)
        except KeyError:
            pass
