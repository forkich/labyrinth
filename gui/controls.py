import tkinter as tk

import sys
from tkinter import messagebox

sys.path.append("../gui")
from config import Config


class Controls(tk.Frame):
    __COUNT_INV = 10

    def __init__(self, master, **kwargs):

        self.__direction = tk.StringVar()
        self.__direction.trace_add("write", lambda name, index, mode: self.__update_commands())
        self.__index_item = tk.IntVar()
        self.__index_item.trace_add("write", lambda name, index, mode: self.__update_cell())
        self.__item = 'None'
        self.__inventory_items = []

        tk.Frame.__init__(self, master, **kwargs)
        self.__btn_frame = tk.Frame(self)
        self.__cell_frame = tk.Frame(self)

        self.__command = ''
        self.__up = tk.PhotoImage(file='but_up.png')
        self.__down = tk.PhotoImage(file='but_down.png')
        self.__left = tk.PhotoImage(file='but_left.png')
        self.__right = tk.PhotoImage(file='but_right.png')
        self.__cell = tk.PhotoImage(file='cell.png')
        self.__bomb = tk.PhotoImage(file='bomb.png')
        self.__bomb = self.__bomb.subsample(2, 2)
        self.__revolver = tk.PhotoImage(file='revolver.png')
        self.__revolver = self.__revolver.subsample(2, 2)

        self.__player = tk.Label(self, text="MASTER", bd=5)
        self.__player.grid(column=0, row=0, sticky="news")
        self.__text = tk.Text(self, width=30, height=1)
        self.__text.bind('<Return>', self.__send_command)
        self.__text.grid(column=1, row=0, sticky="nswe")

        btn_dir_args = dict(
            variable=self.__direction,
            indicatoron=False,
            selectcolor="red",
            width=74,
            height=68
        )

        btn_cell_args = dict(
            image=self.__cell,
            variable=self.__index_item,
            indicatoron=False,
            selectcolor="red",
            width=46,
            height=53
        )

        self.__buttons_cell = []

        self.__btn_n = tk.Radiobutton(self.__btn_frame, image=self.__up,
                                      value='n', **btn_dir_args)
        self.__btn_s = tk.Radiobutton(self.__btn_frame, image=self.__down,
                                      value='s', **btn_dir_args)
        self.__btn_w = tk.Radiobutton(self.__btn_frame, image=self.__left,
                                      value='w', **btn_dir_args)
        self.__btn_e = tk.Radiobutton(self.__btn_frame, image=self.__right,
                                      value='e', **btn_dir_args)
        self.__btn_start = tk.Button(self.__btn_frame, text="Start",
                                     command=self.__start)
        self.__btn_walk = tk.Button(self.__btn_frame, text="Walk",
                                    command=self.__compose_command)
        self.__btn_use = tk.Button(self.__btn_frame, text="Use",
                                   command=self.__use_command)
        self.__btn_turn = tk.Button(self.__btn_frame, text="Make a turn", command=self.__make_turn)

        self.__cell_frame.grid(column=10, row=1, columnspan=10, sticky="nswe")
        for i in range(self.__COUNT_INV):
            self.__buttons_cell.append(tk.Radiobutton(self.__cell_frame, **btn_cell_args, value=i))
            self.__buttons_cell[i].grid(column=i + 8, row=1)

        self.__btn_n.select()
        self.__btn_frame.grid(column=0, row=1, columnspan=2, sticky="nswe")

        self.__btn_n.grid(column=1, row=0, sticky="nswe")
        self.__btn_s.grid(column=1, row=2, sticky="nswe")
        self.__btn_w.grid(column=0, row=1, sticky="nswe")
        self.__btn_e.grid(column=2, row=1, sticky="nswe")
        self.__btn_walk.grid(column=4, row=1, sticky="nswe")
        self.__btn_walk['state'] = 'disabled'
        self.__btn_start.grid(column=5, row=0, sticky="nswe")
        self.__btn_use.grid(column=4, row=2, sticky="nswe")
        self.__btn_use['state'] = 'disabled'
        self.__btn_turn.grid(column=4, row=0, sticky="nswe")
        self.__btn_turn['state'] = 'disabled'

        # for i in range(0, 2):
        #     self.grid_rowconfigure(i, weight=1)
        # self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(1, weight=1)

        # for i in range(0, 4):
        #     self.__btn_frame.grid_columnconfigure(i, weight=1)
        # for i in range(0, 2):
        #     self.__btn_frame.grid_rowconfigure(i, weight=1)

        self.__btn_frame.grid_columnconfigure(3, weight=1)
        self.__btn_frame.grid_columnconfigure(6, weight=1)
        self.__btn_frame.grid_rowconfigure(3, weight=1)

        # self.__cell_frame.grid_columnconfigure(7, weight=3)

    def __update_commands(self):
        player = self.__player.cget("text")
        if player == "MASTER":
            return
        command = "walk " + player + ' ' + self.__direction.get()
        self.__btn_walk.config(text=command)
        if self.__item != 'None':
            self.__btn_use['state'] = "normal"
            command = "use " + player + ' ' + self.__item + ' ' + self.__direction.get()
            self.__btn_use.config(text=command)

    def __update_cell(self):
        self.__item = str(self.__inventory_items[self.__index_item.get()])
        if self.__item.startswith('Revolver'):
            self.__item = 'Revolver'
        self.__update_commands()

    def __compose_command(self):
        self.__command = "walk " + self.__player.cget("text") + ' ' + self.__direction.get()
        self.__btn_turn['state'] = 'normal'

    def __use_command(self):
        self.__command = "use " + self.__player.cget("text") + ' ' + self.__item + ' ' + self.__direction.get()
        self.__btn_turn['state'] = 'normal'

    def update_inventory(self):
        self.__inventory_items = self.get_current_inventory_items()
        if self.__inventory_items is None:
            return
        for i in range(self.__COUNT_INV):
            if str(self.__inventory_items[i]) == 'Bomb':
                self.__buttons_cell[i].config(image=self.__bomb)
                self.__buttons_cell[i]['state'] = "normal"
            elif str(self.__inventory_items[i]).startswith('Revolver'):
                self.__buttons_cell[i].config(image=self.__revolver)
                self.__buttons_cell[i]['state'] = "normal"
            else:
                self.__buttons_cell[i].config(image=self.__cell)
                self.__buttons_cell[i]['state'] = "disabled"

    def __make_turn(self):
        Config.execute_command(self.__command)
        self.__btn_turn['state'] = 'disabled'
        self.__btn_use['state'] = 'disabled'

        if self.__command.startswith('use'):
            self.__update_cell()

    def get_current_inventory_items(self):
        player = self.__player.cget("text")
        if player != 'MASTER':
            return Config.get_instance().get_inventory_items(player)

    def __start(self):
        Config.get_instance().commands.start()
        self.update_inventory()
        self.__buttons_cell[0].select()

    def activate_game_mode(self):
        self.__btn_start['state'] = "disabled"
        self.__btn_use['state'] = "disabled"
        self.__btn_walk['state'] = "normal"

    def set_player(self, player):
        self.__player.config(text=player, bg=Config.get_instance().players[player])
        self.update_inventory()
        self.__update_commands()

    def __send_command(self, event):
        try:
            Config.execute_command(self.__text.get("1.0", tk.END))
        except:
            messagebox.showerror(None, sys.exc_info()[1])
        self.__text.delete("1.0", tk.END)

    def set_direction(self, event, direction):
        pass
