import sys

import asyncio
import aioxmpp
import xmlrpc
import xmlrpc.server
from xmlrpc.client import Fault, dumps, loads

import re
import json

from xml_rpc.xmlrpc_utils import MessageType, CallData

class _AsyncMethod:
    # some magic to bind an XML-RPC method to an RPC server.
    # supports "nested" methods (e.g. examples.getStateName)
    def __init__(self, send, name):
        self.__send = send
        self.__name = name

    def __getattr__(self, name):
        return _AsyncMethod(self.__send, "%s.%s" % (self.__name, name))

    async def __call__(self, *args):
        return_value = await self.__send(self.__name, args)
        return return_value

class JabberXMLRPCManager(xmlrpc.server.SimpleXMLRPCDispatcher):
    # Warning: this is for debugging purposes only! Never set this to True in
    # production code, as will be sending out sensitive information (exception
    # and stack trace details) when exceptions are raised inside
    # SimpleXMLRPCRequestHandler.do_POST
    _send_traceback_header = False

    def __init__(self,
                 timeout=10,
                 encoding=None,
                 verbose=False,
                 allow_none=False,
                 use_datetime=False,
                 logRequests=True):

        xmlrpc.server.SimpleXMLRPCDispatcher.__init__(
            self,
            allow_none,
            encoding)

        self.__client = None

        self.__timeout = timeout

        self.__encoding = encoding or 'utf-8'
        self.__verbose = verbose
        self.__allow_none = allow_none
        self.__use_datetime = use_datetime
        self.logRequests = logRequests

        self.__next_request_id = 0
        self.__requests_and_responses = {}

    def _marshaled_dispatch(self, data, dispatch_method=None, path=None):
        """Dispatches an XML-RPC method from marshalled (XML) data.

        XML-RPC methods are dispatched from the marshalled (XML) data
        using the _dispatch method and the result is returned as
        marshalled data. For backwards compatibility, a dispatch
        function can be provided as an argument (see comment in
        SimpleXMLRPCRequestHandler.do_POST) but overriding the
        existing method through subclassing is the preferred means
        of changing method dispatch behavior.
        """

        try:
            params, method = loads(data, use_builtin_types=False)

            # generate response
            if dispatch_method is not None:
                response = dispatch_method(method, params)
            else:
                response = self._dispatch(method, params)
            # wrap response in a singleton tuple
            response = (response,)
            response = dumps(response, methodresponse=1,
                             allow_none=self.allow_none,
                             encoding=self.encoding)
        except Fault as fault:
            response = dumps(fault, allow_none=self.allow_none,
                             encoding=self.encoding)
        except:
            # report exception back to server
            exc_type, exc_value, exc_tb = sys.exc_info()
            try:
                response = dumps(
                    Fault(1, "%s:%s" % (exc_type, exc_value)),
                    encoding=self.encoding, allow_none=self.allow_none,
                )
            finally:
                # Break reference cycle
                exc_type = exc_value = exc_tb = None

        return response

    def connect(self, jid, password, to):

        self.__jid = jid
        self.__to = to
        self.__client = (aioxmpp.PresenceManagedClient(
            jid,
            aioxmpp.make_security_layer(password)))

        message_dispatcher = self.__client.summon(
            aioxmpp.dispatcher.SimpleMessageDispatcher
        )
        message_dispatcher.register_callback(
            aioxmpp.MessageType.CHAT,
            None,
            self.__message_received
        )

    def __response_received(self, id_, msg):
        # check if the response is sent by the server to this client?
        try:
            self.__requests_and_responses[id_].response = msg
        except KeyError:
            raise ValueError("Invalid response id")

    def connected(self):
        # TODO catch TimeoutError here?
        return self.__client.connected()

    async def __request(self, methodname, params):
        # call a method on the remote server

        request_contents = xmlrpc.client.dumps(
            params,
            methodname,
            encoding=self.__encoding,
            allow_none=self.__allow_none)
        request_msg = aioxmpp.Message(
            type_=aioxmpp.MessageType.CHAT,
            to=self.__to,
        )
        current_id = self.__next_request_id
        request_msg.body[None] = request_contents
        # use the message subject to store the extra data
        extra_data = json.dumps({"id": current_id, "type": MessageType.REQUEST})
        request_msg.subject[None] = extra_data
        # enqueue can raise ConnectionError
        self.__client.enqueue(request_msg)
        self.__requests_and_responses[self.__next_request_id] = \
            CallData(request_msg, None)
        self.__next_request_id += 1
        time_left = self.__timeout
        while (self.__requests_and_responses[current_id].response is None
               and time_left > 0):
            await asyncio.sleep(1)
            time_left -= 1
        if self.__requests_and_responses[current_id].response is None:
            raise TimeoutError(f"{self.__jid}'s request"
                               f" (sent to {self.__to}) did not get a response")
        return_value = self.__handle_response(current_id)
        return return_value

    def __handle_response(self, id_):
        response = self.__requests_and_responses[id_].response
        try:
            response_tuple = xmlrpc.client.loads(response.body.any(),
                                                 self.__use_datetime)

            return response_tuple[0][0]
        except xmlrpc.client.Fault as e:
            print("ERROR")
            print(f"Fault string: {e.faultString}")
            print(f"Fault code: {e.faultCode}\n")
            # FIXME doesn't work correctly with some exceptions
            regexp = re.compile("<class '(.+)'>:'(.+)'")
            match = regexp.match(e.faultString)
#            raise eval(f"{match.group(1)}(\'{match.group(2)}\')") from None
        finally:
            self.__requests_and_responses.pop(id_)

    def __getattr__(self, name):
        # magic method dispatcher
        return _AsyncMethod(self.__request, name)

    def __request_received(self, id_, msg):
        response_contents = self._marshaled_dispatch(msg.body.any())
        response = msg.make_reply()
        response.body[None] = response_contents
        response.subject[None] = json.dumps({"id": id_, "type": MessageType.RESPONSE})
        # enqueue can raise ConnectionError
        self.__client.enqueue(response)

    def __message_received(self, msg):
        if msg.body and msg.subject:
            subject_contents = msg.subject.any()
            print(f"The message received: {msg}\nContents:{msg.body.any()}"
                  f"Subject: {subject_contents}")
            subject_contents_dict = json.loads(subject_contents)
            id_ = subject_contents_dict["id"]
            message_type = subject_contents_dict["type"]
            if message_type == MessageType.REQUEST:
                self.__request_received(id_, msg)
            else:
                self.__response_received(id_, msg)
