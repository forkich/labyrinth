# layoutwidget.py

import tkinter as tk
import tkinter.font as tkFont

from modules.gameinfo import GameInfo

from pubsub import pub

class LayoutWidget(tk.Frame):

    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.__master = master
        self.__canvas = tk.Canvas(self, bg="black", width=0)
        self.__canvas.pack(fill=tk.BOTH, expand="yes")
        self.__font = tkFont.Font(family="Mono", size=20)
        pub.subscribe(
            self.__player_position_callback,
            "Player.position"
        )
        pub.subscribe(
            self.__labyrinth_map_callback,
            "Labyrinth.map"
        )
        self.__terrain = None

    @property
    def font(self):
        return self.__font

    def __player_position_callback(self, name, x, y):
        players = [
            jid for jid in GameInfo.users.keys()
            if getattr(GameInfo.users[jid], "nick", "") == name
        ]
        if len(players) == 1:
            color = GameInfo.users[players[0]].color
        else:
            color = "#ffffff"

        px_w = self.__font.measure("+###")
        px_h = 2 * self.__font.metrics("linespace")
        self.__canvas.delete(name)
        self.__canvas.create_polygon(
            px_w * (x + 0.25), px_h * (y + 0.5),
            px_w * (x + 0.25), px_h * (y + 0.1),
            px_w * (x + 0.75), px_h * (y + 0.2),
            px_w * (x + 0.25), px_h * (y + 0.3),
            fill=color,
            outline=color,
            tags=name
        )

    def __labyrinth_map_callback(self, size_x, size_y, layout):
        if self.__terrain:
            self.__canvas.delete(self.__terrain)
        self.__terrain = self.__canvas.create_text(
            0,
            0,
            font=self.__font,
            anchor="nw",
            fill="gray",
            text=layout
        )
        bbox = self.__canvas.bbox(self.__terrain)
        w = bbox[2] - bbox[0] + self.__font.measure("___")
        h = bbox[3] - bbox[1] + self.__font.metrics("linespace")
        self.__canvas.configure(width=w, height=h)

