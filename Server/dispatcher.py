from pubsub import pub


class Dispatcher:

    def __init__(self):
        self.__callbacks = {
            "Command": self.handler_Command,
            "Response": self.handler_Response
        }
        self.__players = []
        self.__turn = 0
        self.__msg_id = 0
        self.__is_started = False
        pub.subscribe(self.handler, pub.ALL_TOPICS)
        # FIXME: message 'Labyrinth.players' is not sent any longer
        pub.subscribe(self.handler_Labyrinth, "Labyrinth.players")

    def handler(self, topicObj=pub.AUTO_TOPIC, **kwargs):
        topic = topicObj.getName().split('.')
        if topic[0] in self.__callbacks:
            self.__callbacks[topic[0]](topic[1], **kwargs)

    def handler_Command(self, command, **kwargs):
        # print(f"Dispatcher:Command.{command}", kwargs)
        if self.__is_started and command in ["walk", "use"]:
            player = kwargs["player"]
            if player == self.__get_current_player():
                self.__msg_id = kwargs["id"]
            else:
                raise ValueError(f"ERROR: Not {player}'s turn")


    def handler_Response(self, command, **kwargs):
        if command == "start" and kwargs["rc"]:
            self.__is_started = True
            self.__next_turn()
            return
        if self.__is_started and command in ["walk", "use"]:
            msg_id = kwargs["id"]
            if msg_id == self.__msg_id:
                self.__next_turn()
            else:
                raise ValueError(f"ERROR: expected id={self.__msg_id} got {msg_id}")

    def handler_Labyrinth(self, players):
        self.__players = players

    def __get_current_player(self):
        if len(self.__players) > 0:
            return self.__players[(self.__turn - 1) % len(self.__players)]
        return None

    def __next_turn(self):
        self.__turn += 1
        pub.sendMessage("Game.turn",
                        turn=self.__turn,
                        player=self.__get_current_player())

