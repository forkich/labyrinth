#
# objects.py
#

from pubsub import pub

class ObjBaseMeta(type):

    @property
    def defaults(self):
        return getattr(self, f"_{self.__class__.__name__}__defaults", None)

    @defaults.setter
    def defaults(self, obj):
        self.__defaults = obj

class ObjBase(metaclass=ObjBaseMeta):

    __context = None

    @property
    def context(self):
        return self.__context

    @classmethod
    def set_context(cls, ctx):
        cls.__context = ctx

    def update(self, suffix='', **kwargs):
        topic = self.__class__.__name__
        if suffix:
            topic += '.' + suffix
        pub.sendMessage(topic, **kwargs)

    def __call__(self, *args):
        return self


class Artifact(ObjBase):

    def __init__(self):
        self.__owner = None

    def __str__(self):
        return self.__class__.__name__

    @property
    def name(self):
        return self.__name

    @property
    def is_portable(self):
        return True

    @property
    def owner(self):
        return self.__owner

    @owner.setter
    def owner(self, owner):
        self.__owner = owner

    def __call__(self, name, *args):
        self.__name = name
        return self


class Building(ObjBase):

    def __str__(self):
        return self.__class__.__name__


class Terrain(ObjBase):

    def __str__(self):
        return self.__class__.__name__

