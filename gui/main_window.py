import tkinter as tk

from pubsub import pub

from config import Config
from controls import Controls
from log import Log
from map_canvas import MapCanvas


class MainWindow(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        pub.subscribe(self.process_message, pub.ALL_TOPICS)

        w = 3 * master.winfo_screenwidth() // 4
        h = 3 * master.winfo_screenheight() // 4
        x = (master.winfo_screenwidth() - w) // 2
        y = (master.winfo_screenheight() - h) // 2
        master.geometry("%dx%d+%d+%d" % (w, h, x, y))

        self.__graphics = MapCanvas(self)
        config = Config.get_instance()
        self.__log = Log(self)
        self.__controls = Controls(self)

        self.grid(column=0, row=0, sticky="nswe")
        self.__graphics.grid(column=0, row=0, rowspan=2, sticky="nswe")
        self.__log.grid(column=1, row=0, sticky="nswe")
        self.__controls.grid(column=1, row=1, sticky="nswe")

        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(0, weight=1)

    def process_message(self, topicObj=pub.AUTO_TOPIC, **kwargs):
        suppress_list = []
        topic = topicObj.getName()
        if topic not in suppress_list:

            # generate for all children?
            # self.event_generate(f"<<{topic}>>")

            if topic.startswith("Game.turn"):
                self.__controls.set_player(kwargs["player"])
            if topic.startswith("Response.start"):
                if kwargs["rc"]:
                    self.__controls.activate_game_mode()
            if topic.startswith("Labyrinth.map"):
                self.__graphics.update_layout(kwargs["layout"])
            else:
                self.__log.display_message(f"{topic:<20} {kwargs}")