#
# commands.py
#

from pubsub import pub
import inspect
import sys

import player

class Commands:

    __CMD_PREFIX = "Command"
    __RSP_PREFIX = "Response"

    def __init__(self, labyrinth=None):
        self.__labyrinth = labyrinth
        self.__is_started = False
        self.__commands = {}
        self.__id_counter = 1

        if self.__labyrinth:
            pub.subscribe(self, self.__CMD_PREFIX)
        else:
            pub.subscribe(self, self.__RSP_PREFIX)

        for cmd, func in inspect.getmembers(self, inspect.ismethod):
            if not cmd.startswith("_"):
                self.__commands[cmd] = func

    # Command wrapper for pub/sub
    def __call__(self, topicObj=pub.AUTO_TOPIC, **kwargs):
        if self.__labyrinth:  # subscriber mode
            topic_list = topicObj.getName().split('.')
            command = topic_list[-1]
            if command == "remote":
                command = topic_list[-2]
            if command in self.__commands:
                msg_id = 0
                if "id" in kwargs:
                    msg_id = kwargs["id"]
                    del kwargs["id"]
                try:
                    rc, msg = self.__commands[command](**kwargs)
                except:
                    rc = False
                    msg = sys.exc_info()[1]
                pub.sendMessage(self.__RSP_PREFIX + '.' + command, id=msg_id, rc=rc, msg=msg)
        else:  # publisher mode
            if topicObj != pub.AUTO_TOPIC:
                response = topicObj.getName().split('.')[-1]
            else:
                command = inspect.stack()[1][0].f_code.co_name
                kwargs = dict(id=self.__id_counter, **kwargs)
                self.__id_counter += 1
                pub.sendMessage(self.__CMD_PREFIX + '.' + command, **kwargs)

    # TODO: Implement custom exceptions
    def __check_started(self, req_started=True):
        if not self.__labyrinth:  # skip check on publisher side
            return
        if req_started and not self.__is_started:
            raise EnvironmentError("Game not started")
        if not req_started and self.__is_started:
            raise EnvironmentError("Game already started")

    def __publisher(self):
        pass

    def __start(self):
        self.__is_started = True

    def __subscriber(self, topicObj=pub.AUTO_TOPIC, **kwargs):
        pass

    #
    # command.admin
    #
    def player(self, name, x, y):
        """Create new player 'player', settle at position (x, y)"""
        if self.__labyrinth:
            self.__check_started(False)
            player.Player(name, int(x), int(y))
            return True, name
        else:
            self.__call__(name=name, x=x, y=y)

    def start(self):
        """Start the game"""
        if self.__labyrinth:
            self.__check_started(False)
            self.__is_started = True
            return True, "Game started"
        else:
            self.__call__()

    #
    # command.player
    #
    def walk(self, player, direction):
        """ Tell the player 'player' to walk in the direction 'direction' """
        if self.__labyrinth:
            self.__check_started()
            rc, msg = self.__labyrinth.player_walk(player, direction)
            return rc, msg
        else:
            self.__call__(player=player, direction=direction)

    def use(self, player, item, direction):
        """Player 'player': use inventory item 'item' in direction 'direction'"""
        if self.__labyrinth:
            self.__check_started()
            return self.__labyrinth.player_use(player, item, direction)
        else:
            self.__call__(player=player, item=item, direction=direction)
