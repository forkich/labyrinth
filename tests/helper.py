#!/usr/bin/env python3

import sys
import inspect

from colors import color

class Helper:

    def __init__(self, module):
        for name, func in inspect.getmembers(sys.modules[module],
                                             inspect.isfunction):
            if name.startswith("test_"):
                print(color(name, fg="black", bg="white"))
                func()

if __name__ == "__main__":
    Helper(__name__)

