import tkinter as tk
from tkinter import ttk
import sys
import tkinter.font as tkFont
from pubsub import pub

sys.path.append("../gui")
from config import Config


class MapCanvas(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.__master = master
        self.__canvas = tk.Canvas(self, bg="black")
        self.__canvas.pack(fill=tk.BOTH, expand="yes")
        self.__font = tkFont.Font(family="Mono", size=20)
        pub.subscribe(self.__player_position_callback, "Player.position")
        self.__canvas.bind("<Button-3>", self.__popup)
        self.__terrain = None
        self.update_layout(Config.get_instance().labyrinth_layout)

    @property
    def font(self):
        return self.__font

    def __popup(self, event):
        popup = tk.Menu(self.__master, tearoff=0)
        popup.add_command(label="Add Player",
                          command=lambda: AddPlayerDialog(self, event.x_root,
                                                          event.y_root,
                                                          event.x, event.y))
        try:
            popup.tk_popup(event.x_root, event.y_root, 0)
        finally:
            popup.grab_release()

    def __player_position_callback(self, name, x, y):
        self.show_player(name, x, y, Config.get_instance().players[name])

    def update_layout(self, layout):
        if self.__terrain:
            self.__canvas.delete(self.__terrain)
        self.__terrain = self.__canvas.create_text(0, 0,
            font=self.__font,
            anchor="nw",
            fill="gray",
            text=layout)
        bbox = self.__canvas.bbox(self.__terrain)
        w = bbox[2] - bbox[0] + self.__font.measure("___")
        h = bbox[3] - bbox[1] + self.__font.metrics("linespace")
        self.__canvas.configure(width=w, height=h)

    def show_player(self, name, x, y, color):
        px_w = self.__font.measure("+###")
        px_h = 2 * self.__font.metrics("linespace")
        self.__canvas.delete(name)
        self.__canvas.create_polygon(
            px_w * (x + 0.25), px_h * (y + 0.5),
            px_w * (x + 0.25), px_h * (y + 0.1),
            px_w * (x + 0.75), px_h * (y + 0.2),
            px_w * (x + 0.25), px_h * (y + 0.3),
            fill=color,
            outline=color,
            tags=name)

class AddPlayerDialog(tk.Toplevel):

    def __init__(self, parent, x, y, map_x, map_y):
        super().__init__(parent)
        self.__parent = parent

        self.__player_name = tk.StringVar()
        self.__player_color = tk.StringVar()

        self.geometry("+%d+%d" % (x, y))
        self.title("Add Player")

        colors = ["#ff0000", "#ffff00", "#00ff00", "#00ffff", "#0000ff", "#ff00ff"]

        ttk.Label(self, text="Name").grid(row=0, column=0, sticky="news")
        player_name = ttk.Entry(self, textvariable=self.__player_name)
        player_name.grid(row=0, column=1, columnspan=len(colors) - 1, sticky="news")
        player_name.bind('<Return>', self.__apply)
        player_name.focus_set()

        players = Config.get_instance().players
        lookup = dict(zip(players.values(), players.keys()))

        for ci in range(len(colors)):
            label, state = (lookup[colors[ci]], tk.DISABLED) if colors[ci] in lookup else (colors[ci], tk.NORMAL)
            btn_player_color = tk.Radiobutton(self,
                bg="black",
                text=label,
                state=state,
                fg=colors[ci],
                activebackground=colors[ci],
                disabledforeground=colors[ci],
                selectcolor=colors[ci],
                value=colors[ci],
                variable=self.__player_color,
                indicatoron=False,
                takefocus=False,
                width=9,
                height=2)
            btn_player_color.grid(row=1, column=ci)
            if not self.__player_color.get() and state == tk.NORMAL:
                btn_player_color.select()

        btn_create = ttk.Button(self, text="Create", command=self.__apply)
        btn_create.grid(row=2,
                        column=0,
                        sticky="news",
                        columnspan=len(colors))

        self.__x = map_x // parent.font.measure("+###")
        self.__y = map_y // (2 * parent.font.metrics("linespace"))

    def __apply(self, src=None):
        config = Config.get_instance()
        config.add_player(self.__player_name.get(), self.__player_color.get())
        config.commands.player(self.__player_name.get(),
                              self.__x,
                              self.__y)
        self.destroy()

