from dataclasses import dataclass
from enum import IntEnum
from typing import Optional
from aioxmpp import Message


class MessageType(IntEnum):
    REQUEST = 0
    RESPONSE = 1


@dataclass
class CallData:
    request: Message
    response: Optional[Message]
