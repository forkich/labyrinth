#!/usr/bin/env python3

from helper import *

import sys
sys.path.append("../Server")

from pubsub import pub

#
# Load rules via direct "exec" instead of import - simulate game mode
#
from labyrinth import Labyrinth
from player import Player
from objects import *

for file_name in ["rules.py", "bindings.py"]:
    with open(file_name) as f:
        exec(compile(f.read(), file_name, "exec"))

def layout_from_file(path):
    with open(path) as f:
        return f.read()

def create_empty_map(rows, cols):
    def border_line(wall):
        return "   +" + ((wall * 3) + '+') * cols + '\n'

    ret = ("   "
           + ''.join([f" {c:2d} " for c in range(1, cols + 1)])
           + '\n'
           + border_line('#')
           )

    for r in range(1, rows + 1):
        ret += f"{r:2d} #" + ' ' * (4 * cols - 1) + "#\n"
        ret += border_line('#' if r == rows else ' ')

    return ret.rstrip()  # strip trailing newline

# snoop pubsub messages, print if topic is not in suppress list
def snoop(topicObj=pub.AUTO_TOPIC, **kwargs):
    suppress_list = [
        "Labyrinth.map"
    ]
    topic = topicObj.getName()
    if topic not in suppress_list:
        print(f"{topic:<20} {kwargs}")

pub.subscribe(snoop, pub.ALL_TOPICS)

def test_layout_loader():
    l = Labyrinth(
"""
     1   2   3   4
   +###+###+###+###+
 1 # w1      p2  H #
   +   +---+---+   +
 2 # p1| T   v2    #
   +   +---+---+---+
 3   v1      w2
   +---+---+---+   +
 4 # A   v3  p3    #
   +###+###+###+###+
""",
        {
            'A': Arsenal(10 * [Bomb()]),
        })
    assert l.size_y == 4
    assert l.size_x == 4

    print(l)
    for k, v in sorted(l.terrain.items()):
        print(f"Ter {k} : {v}")
    for k, v in sorted(l.objects.items()):
        print(f"Obj {k} : {v}")
    for k, v in l.callbacks.items():
        print(k, v)

    print([k for k, v in l.terrain.items() if isinstance(v, SpawnPoint)])

    p = Player("Dummy", 2, 3)
    # Fall w2 -> w1
    rc, msg = l.player_walk(p.name, 'e')
    assert rc and msg == "Wormhole" and p.pos == (1, 1)
    rc, msg = l.player_walk(p.name, 's')
    assert rc and msg == "Ok"
    # Fall v1 -> v2
    rc, msg = l.player_walk(p.name, 's')
    assert rc and msg == "Wormhole" and p.pos == (3, 2)

    # Pick up treasure
    rc, msg = l.player_walk(p.name, 'w')
    assert rc and p.pos == (2, 2)

    print(l)

def test_01_labyrinth_ctor():
    layout = layout_from_file('layout_01.txt')
    l = Labyrinth(layout)
    assert l.size_y == 8
    assert l.size_x == 8
    l_map = l.map.split('\n')
    expected = layout.rstrip().split('\n')
    assert len(l_map) == len(expected)
    for i in range(len(expected)):
        assert expected[i].rstrip() == l_map[i].rstrip()


def test_02_labyrinth_ctor_size_limits():
    l_4x4 = Labyrinth(create_empty_map(4, 4))
    l_20x20 = Labyrinth(create_empty_map(20, 20))

    for rows, cols in [(3, 4), (4, 3), (21, 4), (4, 21)]:
        try:
            l = Labyrinth(create_empty_map(rows, cols))
            assert False  # ctor above shall raise ValueError
        except ValueError:
            print(sys.exc_info()[1])
        except:
            print(color("Unexpected error", fg="#ff0000", style="bold"))
            raise


def test_03_player_walk():
    l = Labyrinth(layout_from_file('layout_01.txt'))

    p = Player("Lucky", 8, 8, [Treasure()])
    for i in range(3):
        rc, msg = l.player_walk(p.name, 'n')
        assert rc and msg == "Ok"
    assert p.pos == (8, 5)

    rc, msg = l.player_walk(p.name, 'n')
    assert (not rc) and msg == "Wall"

    rc, msg = l.player_walk(p.name, 'e')
    assert rc and msg == "ESCAPED!!!"

    l_5x5 = Labyrinth(create_empty_map(5, 5))
    l_5x5.set_border(1, 3, 'w', ' ')
    l_5x5.set_border(5, 3, 'e', ' ')
    l_5x5.set_border(3, 1, 'n', ' ')
    l_5x5.set_border(3, 5, 's', ' ')
    # print(l_5x5.map)

    p = Player("Lucky", 3, 3, [Treasure()])
    for d in "news":
        p.move_abs(3, 3)
        for i in range(2):
            rc, msg = l_5x5.player_walk(p.name, d)
            assert rc and msg == "Ok"

        rc, msg = l_5x5.player_walk(p.name, d)
        assert rc and msg == "ESCAPED!!!"


def test_04_artifact_bindings():
    l = Labyrinth(layout_from_file('layout_01.txt'))

    p = Player("Unabomber", 5, 4, [Bomb()])
    direction = 'e'
    assert l.get_border(p.pos.x, p.pos.y, direction).b == '|'
    rc, msg = l.player_use(p.name, "Bomb", direction)
    assert rc
    print(msg)
    assert l.get_border(p.pos.x, p.pos.y, direction).b == ' '

    rc, msg = l.player_use(p.name, "Bomb", direction)
    assert not rc
    print(msg)

    p = Player("Gunfighter", 5, 5, [Revolver(2)])
    rc, msg = l.player_use(p.name, "Revolver", 'n')
    assert rc and msg == "Bang!"

    Player("Goof", 3, 5)
    Player("Dupe", 3, 5)

    rc, msg = l.player_use(p.name, "Revolver", 'w')
    assert rc and msg == "Bang!"

    rc, msg = l.player_use(p.name, "Revolver", 'w')
    assert not rc


def test_05_artifact_bindings():
    l = Labyrinth(
        layout_from_file('layout_01.txt')
    )

    p = Player("Lucky", 4, 8)
    assert p.inv.count("Treasure") == 0

    rc, msg = l.player_walk(p.name, 'e')
    assert rc == True
    assert p.pos == (5, 8)
    assert p.inv.count("Treasure") == 1
    print(msg)


def test_06_building_bindings():
    l = Labyrinth(
        layout_from_file('layout_01.txt'),
        {
            'A': Arsenal(2 * [Bomb()])
        }
    )

    p = Player("Armed", 1, 5, [Revolver(2), Bomb()])

    for i in range(2):
        rc, msg = l.player_walk(p.name, 'n')
        assert rc
    assert p.pos == (1, 3)
    assert p.inv.count("Bomb") == 3
    print(msg)


def test_07_portable_artifacts():
    l = Labyrinth(
        layout_from_file('layout_01.txt')
    )

    p1 = Player("Player1", 4, 8)
    assert p1.inv.count("Treasure") == 0
    p2 = Player("Player2", 4, 8)
    assert p2.inv.count("Treasure") == 0

    rc, msg = l.player_walk(p1.name, 'e')
    assert rc
    assert p1.pos == (5, 8)
    assert p1.inv.count("Treasure") == 1
    print(msg)

    rc, msg = l.player_walk(p2.name, 'e')
    assert rc
    assert p2.pos == (5, 8)
    assert p2.inv.count("Treasure") == 0
    print(msg)


def test_08_building_bindings():
    l = Labyrinth(
        layout_from_file('layout_01.txt')
    )
    p = Player("Player1", 1, 1)
    rc, msg = l.player_walk("Player1", "e")
    assert rc
    p.health = 1
    rc, msg = l.player_walk("Player1", "s")
    assert rc
    assert p.health == 10


def test_09_river():
    try:
        try:
            l = Labyrinth("""
     1   2   3   4
   +###+###+###+###+
 1 #             H #
   +   +---+---+   +
 2 #   | T         #
   +   +---+---+---+
 3   e   <   v   <
   +---+   +   +   +
 4 # A   s   >   ^ #
   +###+###+###+###+""")
            assert False
        except ValueError:
            print(sys.exc_info()[1])
        try:
            l = Labyrinth("""
    1   2   3   4
  +###+###+###+###+
1 #             H #
  +   +---+---+   +
2 #   | T         #
  +   +---+---+---+
3   e   <   <   <
  +---+   +---+   +
4 # A   s | >   ^ #
  +###+###+###+###+""")
            assert False
        except ValueError:
            print(sys.exc_info()[1])
        try:
            l = Labyrinth("""
     1   2   3   4
   +###+###+###+###+
 1 #             H #
   +   +---+---+   +
 2 #   | e         #
   +   +   +---+---+
 3   T   ^   <   <
   +---+   +---+   +
 4 # A   s   >   ^ #
   +###+###+###+###+""")
            assert False
        except ValueError:
            print(sys.exc_info()[1])

        try:
            l = Labyrinth("""
     1   2   3   4
   +###+###+###+###+
 1 #             H #
   +   +---+---+   +
 2 #   | e         #
   +   +   +---+---+
 3   T   ^   <   <
   +---+---+---+---+
 4 # A   s   >   ^ #
   +###+###+###+###+""")
            assert False
        except ValueError:
            print(sys.exc_info()[1])

        try:
            l = Labyrinth("""
     1   2   3   4
   +###+###+###+###+
 1 #         <   H #
   +   +---+---+   +
 2 #   | e         #
   +   +   +---+---+
 3   T   ^   <   <
   +---+---+---+   +
 4 # A   s   >   ^ #
   +###+###+###+###+""")
            assert False
        except ValueError:
            print(sys.exc_info()[1])

        l = Labyrinth("""
     1   2   3   4
   +###+###+###+###+
 1 #             H #
   +   +---+---+   +
 2 #   | T         #
   +   +---+---+---+
 3   e   <   <   <
   +---+---+---+   +
 4 # A   s   >   ^ #
   +###+###+###+###+""")
    # TODO river interaction tests
    except:
        print(color("Unexpected error", fg="#ff0000", style="bold"))
        raise


def test_interactions():
    labyrinth = Labyrinth(layout_from_file('layout_01.txt'))
    player1 = "Player1"
    p1 = Player(player1, 1, 1)
    assert p1.name == player1
    assert p1.pos == (1, 1)
    player2 = "Player2"
    p2 = Player(player2, 1, 1)
    assert p2.name == player2
    assert p2.pos == (1, 1)
    labyrinth.player_walk(player1, "w")
    assert p1.pos == (1, 1)
    labyrinth.player_walk(player1, "e")
    assert p1.pos == (2, 1)
    labyrinth.player_walk(player1, "n")
    assert p1.pos == (2, 1)
    labyrinth.player_walk(player1, "s")
    assert p1.pos == (2, 2)


# Execute all locally defined tests if running autonomously (w/o pytest)
if __name__ == "__main__":
    Helper(__name__)
