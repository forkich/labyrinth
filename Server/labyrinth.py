# Labyrinth

from objects import ObjBase
from sequence_objects import River
from player import Player

import typing
import inspect
import re
from pubsub import pub

class Labyrinth(ObjBase):
    __min_cells = 4
    __max_cells = 20
    DIRECTIONS = {
        'n': (0, -1),
        'e': (1, 0),
        'w': (-1, 0),
        's': (0, 1)
    }

    RIVER_CHARACTERS = set("v^<>se")

    class Border(typing.NamedTuple):
        x: int
        y: int
        d: str
        b: str

    def __init__(self, layout, handlers=None):
        self.__callbacks = {}
        self.__objects = {}
        self.__terrain = {}
        self.__river = {}
        Player.registry.clear()
        self.__load_map(layout, handlers if handlers else {})

    def update_map(self):
        ObjBase.update(self, "map",
                       size_x=self.__size_x,
                       size_y=self.__size_y,
                       layout=self.__get_map())

#    def update_players(self):
#        ObjBase.update(self, "players",
#                       players=list(Player.registry.keys()))

    def __load_map(self, layout, handlers):
        map_lines = layout.strip(" \t\n").split('\n')
        self.__size_y = (len(map_lines) - 2) // 2
        self.__size_x = int(map_lines[0].rstrip()[-4:])

        error_message = ("The layout contains an invalid number of "
                         "{0}s: {1}. The {0} number "
                         "must be between {2} and "
                         "{3}")
        if not (self.__min_cells <= self.__size_x <= self.__max_cells):
            raise ValueError(error_message.format("column", self.__size_x,
                                                  self.__min_cells,
                                                  self.__max_cells))
        if not (self.__min_cells <= self.__size_y <= self.__max_cells):
            raise ValueError(error_message.format("row", self.__size_y,
                                                  self.__min_cells,
                                                  self.__max_cells))

        self.__cells = [[] for i in range(self.__size_y)]
        self.__walls = [[] for i in range(len(map_lines) - 1)]
        map_lines.pop(0)

        # build missing handlers from docstrings
        for obj_cls in ObjBase.__subclasses__():
            for sb in obj_cls.__subclasses__():
                if sb.__doc__ and sb.__doc__ not in handlers:
                    obj = sb.__new__(sb)
                    sb.__init__(obj)
                    handlers[sb.__doc__] = obj

        patterns = {}
        for p, obj in handlers.items():
            patterns[re.compile(f"^{p}$")] = obj
        river_start = None
        width = self.__size_x * 4 + 4
        for i in range(len(map_lines)):
            if len(map_lines[i]) < width:
                map_lines[i] += ' ' * (width - len(map_lines[i]))
            if i % 2 == 0:
                self.__walls[i] = [map_lines[i][j]
                                   for j in range(5, width, 4)]
            else:
                self.__walls[i].append(map_lines[i][3])
                for j in range(1, self.__size_x + 1):
                    y = i // 2
                    x = j - 1
                    current_obj = "".join(
                        map_lines[i][j * 4: j * 4 + 3].split())
                    self.__cells[y].append(current_obj)

                    if current_obj != '':
                        if current_obj in Labyrinth.RIVER_CHARACTERS:
                            if current_obj == 's':
                                river_start = (x, y)
                            self.__river[(x, y)] = current_obj
                        else:
                            for p in patterns.keys():
                                if p.match(current_obj):
                                    obj = patterns[p]
                                    if hasattr(obj, "is_portable") and obj.is_portable:
                                        self.__objects[x + 1, y + 1] = obj(current_obj, x + 1, y + 1)
                                        # portable objects are not stored in __cells
                                        self.__cells[y][x] = ''
                                    else:
                                        self.__terrain[x + 1, y + 1] = obj(current_obj, x + 1, y + 1)
                                    break

                    self.__walls[i].append(map_lines[i][j * 4 + 3])
        if river_start:
            self.__process_river(river_start)
        else:
            if self.__river:
                raise ValueError("The river in the layout "
                                 "doesn't have a start")
        self.update_map()

        # set up callbacks
        for obj_cls in ObjBase.__subclasses__():
            obj_cls.set_context(self)
            for sb in obj_cls.__subclasses__():
                for func, callback in inspect.getmembers(sb,
                                                         inspect.isfunction):
                    argspec = inspect.getfullargspec(callback)
                    # set callback if:
                    # - the method takes two args
                    # - second arg has annotation
                    if len(argspec.args) != 2:
                        continue
                    target = argspec.args[1]
                    if target not in argspec.annotations:
                        continue
                    if sb not in self.__callbacks:
                        self.__callbacks[sb] = {}
                    self.__callbacks[sb][
                        argspec.annotations[target]] = callback

    @property  # DEBUG
    def callbacks(self):
        return self.__callbacks

    @property  # DEBUG
    def objects(self):
        return self.__objects

    @property  # DEBUG
    def terrain(self):
        return self.__terrain

    def pick(self, x, y):
        obj = self.__objects.get((x, y))
        if obj:
            del self.__objects[x, y]
        return obj

    def get_players_at_pos(self, x, y):
        ret = []
        for p in Player.registry.values():
            if p.pos.x == x and p.pos.y == y:
                ret.append(p)
        return ret

    def player_walk(self, name, direction):
        player = Player.registry[name]
        # interaction with border
        if self.get_border(player.pos.x, player.pos.y, direction).b in "#-|":
            self.__interact(player, self.__river)
            return False, "Wall"
        player.move_rel(self.DIRECTIONS[direction])
        if (player.pos.x not in range(1, self.__size_x + 1)
                or player.pos.y not in range(1, self.__size_y + 1)):
            return True, "ESCAPED!!!"  # TODO: call custom escape handler
        # interaction with objects at destination
        obj = self.__objects.get((player.pos.x, player.pos.y))
        rc = self.__interact(player, obj) if obj else None
        # TODO interaction with the river
        self.__interact(player, self.__river)
        # interaction with terrain at destination
        obj = self.__terrain.get((player.pos.x, player.pos.y))
        rc = self.__interact(player, obj) if obj else None
        if rc:
            return rc
        return True, "Ok"

    def player_use(self, name, item_name, direction=None):
        player = Player.registry[name]
        item = player.inv.pull(item_name)
        border = self.get_border(player.pos.x, player.pos.y, direction)
        if not item:
            return False, f"No '{item_name}' in inventory"
        return self.__interact(item, border)

    def __process_river(self, start):
        sequence = River()
        rc_dict = {
            '^': ((0, -1), 'n'),
            '>': ((1, 0), 'e'),
            '<': ((-1, 0), 'w'),
            'v': ((0, 1), 's')
        }
        node_count = 0
        current = None
        for k, v in Labyrinth.DIRECTIONS.items():
            neighbouring = (start[0] + v[0], start[1] + v[1])
            wall_y, wall_x = self.__border_indices(start[0] + 1,
                                                   start[1] + 1, k)
            if (neighbouring in self.__river
                    and self.__walls[wall_y][wall_x] not in "#|-"):
                symbol = self.__river[neighbouring]
                if rc_dict[symbol][0] == (-v[0], -v[1]):
                    raise ValueError("Invalid river direction near its start")
                if rc_dict[symbol][0] == v:
                    node_count += 1
                    current = neighbouring
                    sequence.add_node(River.Node(symbol, (neighbouring[0] + 1, neighbouring[1] + 1)),
                                      (start[0] + 1, start[1] + 1))
        if node_count != 1:
            raise ValueError("There must be at least one river character"
                             " pointing away from the rivers's start and not"
                             " separated from it by any walls")
        self.__river.pop(start)
        while self.__river[current] != 'e':
            d = rc_dict[self.__river[current]][0]
            next_node = (current[0] + d[0], current[1] + d[1])
            if next_node in sequence:
                raise ValueError("The river has a loop")

            wall_y, wall_x = self.__border_indices(current[0] + 1,
                                                   current[1] + 1,
                                                   rc_dict[
                                                       self.__river[
                                                           current]][1])
            if (next_node not in self.__river) or (self.__walls[wall_y][wall_x]
                                                   in "#|-"):
                raise ValueError("The river doesn't end with an 'e'")
            next_symbol = self.__river[next_node]
            if next_symbol != 'e':
                next_d = rc_dict[next_symbol][0]
                if d[0] == -next_d[0] and d[1] == -next_d[1]:
                    raise ValueError("Invalid river direction")
            sequence.add_node(River.Node(self.__river[current], (next_node[0] + 1, next_node[1] + 1)),
                              (current[0] + 1, current[1] + 1))
            self.__river.pop(current)
            current = next_node
        sequence.add_node(River.Node(self.__river[current]), (current[0] + 1, current[1] + 1))
        self.__river.pop(current)
        if self.__river:
            raise ValueError("Not all river nodes are properly connected")
        self.__river = sequence

    def __str__(self):
        return self.__get_map()

    def __get_map(self):
        result = ("    "
                  + " ".join(["{:2d} ".format(i + 1)
                              for i in range(self.__size_x)])
                  + '\n')

        def str_from_wall(wall):
            return (' ' + wall
                    + ' ' if wall in "^v><" else 3 * wall) + '+'

        def str_from_cell(x, y):
            cell = self.__cells[y - 1][x - 1]
            o = self.__objects.get((x, y))
            cell += o.name if o else ''
            return cell.center(3, ' ')

        for index in range(self.__size_y):
            result += ("   +"
                       + ''.join([str_from_wall(x)
                                  for x in self.__walls[index * 2]])
                       + '\n'
                       + "{:2d} ".format(index + 1)
                       + ''.join(map(lambda a, b: a + b,
                                     self.__walls[index * 2 + 1][:-1],
                                     [str_from_cell(x + 1, index + 1)
                                      for x in range(self.__size_x)] ))
                       + self.__walls[index * 2 + 1][-1]
                       + '\n')

        result += ("   +"
                   + ''.join([str_from_wall(x)
                              for x in self.__walls[self.__size_y * 2]]))
        return result

    @property
    def size_x(self):
        return self.__size_x

    @property
    def size_y(self):
        return self.__size_y

    @property
    def map(self):
        return self.__get_map()

    def __border_indices(self, x, y, d):
        if not (1 <= x <= self.__size_x and 1 <= y <= self.__size_y):
            raise ValueError(f"Invalid position ({x}, {y})")

        if d == 'n':
            return 2 * y - 2, x - 1
        elif d == 's':
            return 2 * y, x - 1
        elif d == 'w':
            return 2 * y - 1, x - 1
        elif d == 'e':
            return 2 * y - 1, x
        else:
            raise ValueError(f"Invalid direction '{d}'")

    def get_border(self, x, y, d):
        iy, ix = self.__border_indices(x, y, d)
        return Labyrinth.Border(x, y, d, self.__walls[iy][ix])

    def set_border(self, x, y, d, b):
        iy, ix = self.__border_indices(x, y, d)
        self.__walls[iy][ix] = b
        self.update_map()

    def __interact(self, object_a, object_b):
        type_a = type(object_a)
        type_b = type(object_b)
        if type_a in self.__callbacks and type_b in self.__callbacks[type_a]:
            return self.__callbacks[type_a][type_b](object_a, object_b)
        if type_b in self.__callbacks and type_a in self.__callbacks[type_b]:
            return self.__callbacks[type_b][type_a](object_b, object_a)
