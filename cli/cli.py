#!/usr/bin/env python3

import sys

sys.path.append("../Server")

import readline
import shlex
import inspect

from colors import color

import argparse

from commands import Commands
from labyrinth import Labyrinth
from pubsub import pub

do_exit = False

class CommandWrapper:

    def __init__(self, commands):
        self.__command_bindings = {}
        self.__bind_commands(self)
        self.__bind_commands(commands)

    def __bind_commands(self, module):
        for func, obj in inspect.getmembers(module, inspect.ismethod):
            if not func.startswith("_"):
                self.__command_bindings[func] = obj

    def __call__(self, cmd, *cmd_args):
        global do_exit

        if cmd not in self.__command_bindings:
            print(color(f"Invalid command '{cmd}'", fg="#ff0000"))
            return None
        else:
            try:
                topic = 'command.' + cmd
                if cmd in self.__command_bindings:
                    return self.__command_bindings[cmd](*cmd_args)
            except SystemExit:
                do_exit = True
            except:
                print(color(sys.exc_info()[1], fg="#ff0000"))
                return None

    def help(self, command=None):
        """Display help (shows full help if 'command' is not specified)"""

        def print_signature(func, obj):
            argspec = inspect.getfullargspec(obj)
            # position at which optional args begin to appear
            opt_pos = len(argspec.args) - (len(argspec.defaults) if argspec.defaults else 0)
            argsig = [argspec.args[i] if i < opt_pos else '[' + argspec.args[i] + ']' \
                      for i in range(1, len(argspec.args))]  # skip 1st arg which is 'self'
            print(color(func, style="bold"), ' '.join(argsig))

        def print_docstring(docstring):
            print(color("- " + docstring if docstring else '', fg="green"))

        if command:
            if command not in self.__command_bindings:
                print(color(f"ERROR: Invalid command '{command}'", fg="#ff0000"))
            else:
                obj = self.__command_bindings[command]
                print_signature(command, obj)
                print_docstring(inspect.getdoc(obj))
            return

        for func, obj in self.__command_bindings.items():
            print_signature(func, obj)
            print_docstring(inspect.getdoc(obj))

    def quit(self):
        """Quit CLI"""
        print(color("Exiting...", fg="#00ff00"))
        sys.exit()


def snoop(topicObj=pub.AUTO_TOPIC, **kwargs):
    suppress_list = []
    topic = topicObj.getName()
    if topic not in suppress_list:
        if topic == "Labyrinth.map":
            print(kwargs["layout"])
        else:
            print(f"{topic:<20} {kwargs}")



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    for arg in ["layout", "rules", "bindings"]:
        parser.add_argument(arg)
    args = parser.parse_args()
    with open(args.layout) as file:
        layout = file.read().rstrip()
    with open(args.rules) as file:
        rules = file.read()
    with open(args.bindings) as file:
        bindings = file.read()

    exec(compile(rules, args.rules, 'exec'))
    map_bindings = eval(compile(bindings, args.bindings, 'eval'))
    pub.subscribe(snoop, pub.ALL_TOPICS)
    cw = CommandWrapper(
            Commands(
                Labyrinth(layout, map_bindings)
            )
        )

    while not do_exit:
        cmd, *cmd_args = shlex.split(input("> "))

        ret = cw(cmd, *cmd_args)
        # if ret:
        #     print(ret)
