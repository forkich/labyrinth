# gameinfo.py

import os
import sys
import zlib
import json

import configparser
import keyring
import random

from modules.jabberproxy import JabberProxy

# context for rules & bindings
sys.path.append("../Server")
from labyrinth import Labyrinth
from player import Player
from objects import *

CONFIG_FILE_TEMPLATE = {
    "Settings" : {
        "geometry" : ""
    },
    "XMPP" : {
        "jid"  : "",
        "chat" : ""
    }
}

class ConfigObject:

    __instance = None

    class Section:

        def __init__(self, proxy):
            # call super() since self.__setattr__() is overloaded
            super().__setattr__(
                f"_{self.__class__.__name__}__proxy",
                proxy
            )

        def __setattr__(self, attr, value):
            if attr in self.__proxy:
                self.__proxy[attr] = value

        def __getattribute__(self, attr):
            try:
                value = super().__getattribute__(attr)
            except AttributeError:
                value = self.__proxy[attr]
            return value

    def __init__(self, template, path=None):
        self.__path = path
        self.__config_parser = configparser.ConfigParser()
        self.__config_parser.read_dict(template)
        self.load(path)

    # Special rule for 'password' property: use keyring
    @property
    def password(self):
        return keyring.get_password(
            os.path.basename(sys.argv[0]).replace(".py", ""),
            self.XMPP.jid
        )

    @password.setter
    def password(self, pwd):
        keyring.set_password(
            os.path.basename(sys.argv[0]).replace(".py", ""),
            self.XMPP.jid,
            pwd
        )

    @property
    def path(self):
        return self.__path

    @path.setter
    def path(self, value):
        self.__path = value

    def load(self, path=None):
        self.__path = path
        if path:
            self.__config_parser.read(path)
        for sect in self.__config_parser.sections():
            self.__setattr__(
                sect,
                ConfigObject.Section(
                    self.__config_parser[sect]
                )
            )

    def save(self, path=None):
        if path:
            self.__path = path
        else:
            path = self.__path

        with open(path, 'w') as f:
            self.__config_parser.write(f)

    @property
    def parser(self):
        return self.__config_parser

class GameInfoMeta(type):

    def __init__(self, *args, **kwargs):
        self.__users = {}
        self.__config = ConfigObject(CONFIG_FILE_TEMPLATE)
        self.__labyrinth = None
        self.__crc = None

    @property
    def users(self):
        return self.__users

    @property
    def config(self):
        return self.__config

    @property
    def labyrinth(self):
        return self.__labyrinth

    @property
    def crc(self):
        return self.__crc

    def publish(self, topic, **kwargs):
        msg = json.dumps(dict(topic=topic, **kwargs))
        JabberProxy.send(msg)

    def update(self):
        players = [
            jid for jid in self.__users.keys()
            if getattr(self.__users[jid], "nick", "") in Player.registry
        ]
        self.publish(
            "Game.info",
            crc=f"{self.__crc:08x}",
            size_x=self.__labyrinth.size_x,
            size_y=self.__labyrinth.size_y,
            players={
                self.__users[jid].nick : self.__users[jid].color
                for jid in players
            }
        )

    def load_labyrinth(self):
        assert "GameMaster" in self.__config.parser.sections()
        cfg = self.__config.GameMaster

        prerequisites = all(
            map(
                os.path.exists,
                (cfg.layout, cfg.rules, cfg.bindings)
            )
        )

        if prerequisites:
            with open(cfg.layout) as f:
                layout = f.read()
                crc = zlib.crc32(layout.encode("utf-8"))
            with open(cfg.rules) as f:
                rules = f.read()
                crc = zlib.crc32(rules.encode("utf-8"), crc)
            with open(cfg.bindings) as f:
                bindings = f.read()
                crc = zlib.crc32(bindings.encode("utf-8"), crc)
            exec(compile(rules, cfg.rules, "exec"))
            exec(compile(bindings, cfg.bindings, "exec"))
            self.__labyrinth = Labyrinth(layout)
            self.__crc = crc
            self.update()

class GameInfo(metaclass=GameInfoMeta):

    __instance = None

    def __init__(self):
        self.__class__.__instance = self

    class UserInfo:

        def __init__(self, **kwargs):
            self.__dict__ = kwargs

        def __setattr__(self, attr, value):
            if attr == "id":
                raise AttributeError("attribute 'id' is immutable")
            super().__setattr__(attr, value)

        def __str__(self):
            return '{' + ", ".join(
                [f"{k}={v}" for k, v in self.__dict__.items()]
            ) + '}'

        def update(self, **kwargs):
            return self.__dict__.update(**kwargs)

    @classmethod
    def connect(cls):
        JabberProxy.connect(
            GameInfo.config.XMPP.jid,
            GameInfo.config.password,
        )

        JabberProxy.join(
            GameInfo.config.XMPP.chat,
            GameInfo.config.XMPP.jid    # use jid as nick
        )

    @classmethod
    def register_user(cls, jid=None, **kwargs):
        if "jid" not in kwargs:
            kwargs = dict(jid=jid, **kwargs)
        elif jid:
            kwargs["jid"] = jid
        else:
            jid = kwargs["jid"]
        cls.users[jid] = GameInfo.UserInfo(**kwargs)

    @classmethod
    def create_game(cls):
        cls.load_labyrinth()

    @classmethod
    def add_player(cls, jid):
        l = cls.labyrinth
        (x, y), sp = random.choice(
            [
                (pos, terrain) for pos, terrain in l.terrain.items()
                if str(terrain) == "SpawnPoint"
                and not l.get_players_at_pos(pos[0], pos[1])
            ]
        )
        player = Player(cls.users[jid].nick, x, y, sp.__class__.defaults)
        cls.update()
