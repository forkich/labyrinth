#
# rules.py
#

class Bomb(Artifact):

    # callback
    def blast(self, border: Labyrinth.Border):
        if border.b in "-|":
            self.context.set_border(border.x, border.y, border.d, ' ')
        return True, "KABOOM!!!"


class Revolver(Artifact):

    MAX_ROUNDS = 6

    def __init__(self, rounds = MAX_ROUNDS):
        self.rounds = rounds

    def __str__(self):
        return self.__class__.__name__ + f"({self.__rounds:d}/{self.MAX_ROUNDS:d})"

    @property
    def is_reusable(self):
        return True

    @property
    def rounds(self):
        return self.__rounds

    @rounds.setter
    def rounds(self, rounds):
        self.__rounds = min(rounds, self.MAX_ROUNDS)

    def reload(self):
        self.rounds = self.MAX_ROUNDS
        self.owner.inv.update()

    # direction is extracted from border
    def shoot(self, border: Labyrinth.Border):
        if not self.__rounds > 0:
            return False, "Low on ammo"
        self.__rounds -= 1

        # bullet
        x, y, d = border.x, border.y, border.d
        l = self.context
        while x in range(1, l.size_x + 1) \
                and y in range(1, l.size_y + 1):
            # hit wall?
            if l.get_border(x, y, d).b != ' ':
                break

            dx, dy = l.DIRECTIONS[d]
            x += dx
            y += dy

            # hit player(s)?
            players = l.get_players_at_pos(x, y)
            if len(players) > 0:
                # -2 hp if one player in the cell,
                # -1 hp from each player otherwise
                for p in players:
                    p.health -= 2 if len(players) == 1 else 1
                break

        return True, "Bang!"


class Treasure(Artifact):

    "T" # map symbol

    # callback
    def capture(self, player: Player):
        player.inv.push(self.context.pick(player.pos.x, player.pos.y))
        return True, f"Treasure captured by '{player.name}'"


class Arsenal(Building):

    "A" # map symbol

    __ITEM_TYPES = ["Bomb"]

    def __init__(self, items = None):
        if self.__class__.defaults and not items:
            items = self.__class__.defaults
        self.__items = items if items else []
        self.update_status()

    def item_count(self, item):
        return list(map(str, self.__items)).count(str(item))

    def update_status(self):
        status = {}
        for item in self.__ITEM_TYPES:
            status[item] = self.item_count(item)
        self.update('', **status)

    # callback
    def refill(self, player: Player):
        # regular items (Bomb etc. etc.)
        for item in self.__ITEM_TYPES:
            while (player.inv.count(None) > 0
                   and player.inv.count(item) < 3
                   and self.item_count(item) > 0):
                idx = list(map(str, self.__items)).index(item)
                player.inv.push(self.__items.pop(idx))
        # special handling of Revolver ammo
        try:
            player.inv[Revolver].reload()
        except:
            pass
        self.update_status()
        return True, "Refill OK"


class Hospital(Building):

    "H" # map symbol

    def heal(self, player: Player):
        player.health = self.__class__.defaults
        self.update('', **{"Player healed": player.name})
        return True, "Heal OK"


class Wormhole(Terrain):

    "[u-w][1-9]" # regexp pattern for map symbols

    def __init__(self):
        self.__sequence = dict(u=[], v=[], w=[])

    def __str__(self):
        return self.__class__.__name__

    def __call__(self, name, x, y):
        seq = name[0]
        if seq in self.__sequence:
            self.__sequence[seq].append((name, x, y))
            self.__sequence[seq].sort(key = lambda v: v[0])
        return self

    def fall(self, player: Player):
        item = list(filter(
            lambda v: v[1] == player.pos.x and v[2] == player.pos.y,
            [wh for seq in self.__sequence.values() for wh in seq]
        ))[0]
        s = item[0][0] # 1st char of 1st element of 3-tuple
        seq = self.__sequence[s]
        idx = (seq.index(item) + 1) % len(seq)

        player.move_abs(seq[idx][1], seq[idx][2])
        status = f"Player '{player.name}' falls into wormhole"
        self.update('', status=status)
        return True, "Wormhole"


class SpawnPoint(Terrain):

    "p[1-9]" # regexp pattern for map symbols

    def __call__(self, name, x, y):
        return self

