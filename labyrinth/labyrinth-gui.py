#!/usr/bin/env python3

import tkinter as tk
from tkinter import messagebox

import sys
import traceback

import asyncio
import aioxmpp
from datetime import datetime

import argparse
from pubsub import pub
import json

import importlib

# import all modules from packages
for pkg in ["modules", "widgets"]:
    for mod in importlib.import_module(pkg).__all__:
        exec(f"from {pkg}.{mod} import *")

GRID_DEFAULTS = dict(sticky="news", ipadx=5)

class StatusWidget(tk.Frame):

    def __init__(self, master, **kwargs):
        self.__root = master.root
        tk.Frame.__init__(self, master, **kwargs)
        self.__btn_new = tk.Button(
            self,
            text="Create Game",
            command=self.__new_game
        )
        self.__btn_new.grid(row=0, column=0)

        self.__btn_add = tk.Button(
            self,
            text="Add Player",
            command=self.__add_player
        )
        self.__btn_add.grid(row=0, column=1)

        self.grid(sticky="news")

    def __new_game(self):
        NewGameDialog(self.__root)

    def __add_player(self):
        AddPlayerDialog(self.__root)
#        GameInfo.game_enter(GameInfo.config.XMPP.jid, "red")

    def update(self):
        row = 1
        self_jid = GameInfo.config.XMPP.jid
        for jid, user in GameInfo.users.items():
            color = "red" if jid == self_jid else "black"
            if not user.active:
                color = "gray"
            label_jid = tk.Label(self, text=jid, fg=color, anchor=tk.W)
            label_jid.grid(row=row, column=0)
            row += 1

class LogWidget(tk.Frame):

    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        self.__txt = tk.Text(self, height=30, width=50)
        self.__sb = tk.Scrollbar(
            self,
            orient=tk.VERTICAL,
            command=self.__txt.yview)

        self.__txt.config(
            state=tk.DISABLED,
            yscrollcommand=self.__sb.set)

        self.__txt.grid(row=0, column=0, **GRID_DEFAULTS)
        self.__sb.grid(row=0, column=1, sticky="ns")

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

    def display_message(self, msg):
        self.__txt.config(state=tk.NORMAL)
        self.__txt.insert(tk.END, str(msg) + '\n')
        self.__txt.see(tk.END)
        self.__txt.config(state=tk.DISABLED)

class CommandWidget(tk.Frame):

    def __init__(self, master, callback, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        self.__callback = callback

        self.__text = tk.Text(self, width=30, height=1)
        self.__text.bind('<Return>', self.__post)
        self.__text.grid(row=0, column=0, **GRID_DEFAULTS)

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

    def __post(self, event):
        try:
            self.__callback(self.__text.get("1.0", tk.END).strip())
        except:
            messagebox.showerror(None, sys.exc_info()[1])
        self.__text.delete("1.0", tk.END)

class MainWindow(tk.Frame):

    def __init__(self, root, **kwargs):
        self.__root = root
        tk.Frame.__init__(self, root, **kwargs)

        geometry = GameInfo.config.Settings.geometry
        if not geometry:
            w = 3 * root.winfo_screenwidth() // 4
            h = 3 * root.winfo_screenheight() // 4
            x = (root.winfo_screenwidth() - w) // 2
            y = (root.winfo_screenheight() - h) // 2
            geometry = "%dx%d+%d+%d" % (w, h, x, y)
        root.geometry(geometry)

        self.__wgt_layout = LayoutWidget(self)
        self.__wgt_status = StatusWidget(self)
        self.__wgt_log = LogWidget(self)
        self.__wgt_cmd = CommandWidget(self, self.__exec)

        self.__wgt_layout.grid(
            row=0,
            column=0,
            rowspan=3,
            sticky="news"
        )
        self.__wgt_status.grid(row=0, column=1, **GRID_DEFAULTS)
        self.__wgt_log.grid(row=1, column=1, **GRID_DEFAULTS)
        self.__wgt_cmd.grid(row=2, column=1, **GRID_DEFAULTS)

        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)

        self.pack(fill=tk.BOTH, expand="yes")
        root.title(GameInfo.config.XMPP.jid)

    @property
    def root(self):
        return self.__root

    @property
    def logger(self):
        return self.__wgt_log

    @property
    def status(self):
        return self.__wgt_status

    def __exec(self, msg):
        self.__wgt_log.display_message("> " + msg)
        JabberProxy.send(msg)

class JabberEventDispatcher:

    def __init__(self, application):
        self.__app = application
        self.__handlers = {
            "Game.enter" : self.__on_game_enter,
            "Game.info"  : self.__on_game_info
        }
        self.__proxy = JabberProxy(
            on_private   = self.__on_private,
            on_message   = self.__on_message,
            on_muc_enter = self.__on_muc_enter,
            on_exit      = self.__on_exit,
            on_leave     = self.__on_leave,
            on_join      = self.__on_join
       )

    #
    # aioxmpp handlers
    #
    def __on_private(self, message):
        self.__message_handler(
            message,
            str(message.from_.bare())
        )

    def __on_message(self, message, member, source, **kwargs):
        self.__message_handler(
            message,
            member.conversation_jid.resource
        )

    def __on_muc_enter(self, presence, occupant, **kwargs):
        self.__on_join(occupant, **kwargs)

    def __on_exit(self, **kwargs):
        pass

    def __on_join(self, member, **kwargs):
        print("{} ### {:22} : {:22} joined".format(
            datetime.utcnow().isoformat(),
            GameInfo.config.XMPP.jid,
            member.nick or member.direct_jid
        ))
        if member.nick not in GameInfo.users:
            GameInfo.register_user(
                member.nick,
                game=None,
                active=True
            )
        else:
            GameInfo.users[member.nick].active = True
        self.__app.status.update()

    def __on_leave(self, member, muc_leave_mode=None, **kwargs):
        print("{} ### {:22} : {:22} left".format(
            datetime.utcnow().isoformat(),
            GameInfo.config.XMPP.jid,
            member.nick or member.direct_jid
        ))
        GameInfo.users[member.nick].active = False
        self.__app.status.update()

    def __message_handler(self, message, from_jid):
        body = message.body.lookup(
            [aioxmpp.structs.LanguageRange.fromstr("*")]
        )
        self.__app.logger.display_message(f"<{from_jid}:{body}")

        msg = json.loads(body)
        topic = msg["topic"]
        params = {k: v for k, v in msg.items() if k != "topic"}

        if topic in self.__handlers:
            self.__handlers[topic](from_jid, **params)
        else:
            pub.sendMessage(
                topic,
                **dict({"jid": from_jid}, **params)
            )

    #
    # Game specific event handlers
    #
    def __on_game_enter(self, jid, **kwargs):
        GameInfo.users[jid].update(**kwargs)
        if GameInfo.labyrinth:
            print(f"Game.enter  CRC={GameInfo.crc:08x} Player=", GameInfo.users[jid])
            GameInfo.add_player(jid)

    def __on_game_info(self, jid, **kwargs):
        # TODO: register jid as game master
        pass

async def run_tk(root):
    exit_flag = False

    def on_exit():
        nonlocal exit_flag
        exit_flag = True
        JabberProxy.stop()
        GameInfo.config.Settings.geometry = root.geometry()
        GameInfo.config.save()
        root.destroy()

    root.protocol("WM_DELETE_WINDOW", on_exit)

    if not JabberProxy.is_connected():
        if all((
            GameInfo.config.XMPP.jid,
            GameInfo.config.XMPP.chat,
            GameInfo.config.password
        )):
            GameInfo.connect()
        else:
            JabberConnectionDialog(root)

    while not exit_flag:
        await asyncio.sleep(0.05)
        root.update()

def listener(topicObj=pub.AUTO_TOPIC, **kwargs):
    topic = topicObj.getName()
    if topic == "Labyrinth.map":
        pass
    else:
        print(topic, kwargs)

#
# main
#
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config")
    args = parser.parse_args()
    pub.subscribe(listener, pub.ALL_TOPICS)

    if args.config:
        GameInfo.config.load(args.config)

    app = MainWindow(tk.Tk())
    dispatcher = JabberEventDispatcher(app)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            asyncio.gather(
                JabberProxy.run(),
                run_tk(app.root)
            )
        )
    except:
        print("Unexpected error:", sys.exc_info()[0])
        print(traceback.format_exc())

    loop.close()
