#!/usr/bin/env python3

from helper import *

import sys
sys.path.append("../Server")

import typing

from pubsub import pub
import json
import time

from player import *
from labyrinth import Labyrinth


class Artifact:
    def __str__(self):
        return self.__class__.__name__


class Bomb(Artifact):
    pass


class Logger:
    __log = []

    class LogEntry(typing.NamedTuple):
        tm: int
        topic: str
        msg: object

    def __init__(self):
        self.clear()
        pub.subscribe(Logger.capture, pub.ALL_TOPICS)

    def __len__(self):
        return len(self.__log)

    def __getitem__(self, index):
        entry = self.__log[index]
        return Logger.LogEntry(*entry)

    @classmethod
    def capture(cls, topicObj=pub.AUTO_TOPIC, **kwargs):
        cls.__log.append((time.time(),
                          topicObj.getName(),
                          kwargs)
                         )

    @classmethod
    def dump(cls):
        for tm, topic, msg in cls.__log:
            print(time.strftime("[%y-%m-%d %H:%M:%S]", time.gmtime(tm)),
                  f"{topic:<20}",
                  msg)

    @classmethod
    def clear(cls):
        cls.__log = []


def layout_from_file(path):
    with open(path) as f:
        layout = f.read()
    return layout.rstrip()


def test_PlayerUpdate():
    log = Logger()
    assert len(log) == 0
    pl1 = Player("pl1", 1, 1, 3 * [Bomb()])
    assert len(log) == 2
    assert log[0].topic == "Player.status"
    assert log[1].topic == "Player.position"

    pl1.health = 5
    pl1.health += 2
    assert len(log) == 4

    pl1.inv.pull("Bomb")
    inv = json.loads(log[-1].msg["inv"])
    assert inv[0] == None
    assert inv[1] == "Bomb"


def test_LabyrinthUpdate():
    log = Logger()
    assert len(log) == 0
    l = Labyrinth(layout_from_file('layout_01.txt'))
    assert len(log) >= 1
    print(log[-1].msg["layout"])
    l.set_border(1, 1, 's', ' ')
    print(log[-1].msg["layout"])
    log.clear()

    for pl in range(1, 9):
        name = f"Player_{pl}"
        Player(name, pl, pl, 3 * [Bomb()])

        assert log[-2].topic == "Player.status"
        assert log[-2].msg["name"] == name

        assert log[-1].topic == "Player.position"
        assert log[-1].msg["name"] == name
        assert log[-1].msg["x"] == pl
        assert log[-1].msg["y"] == pl

    assert len(log) == 2 * 8

# Execute all locally defined tests if running autonomously (w/o pytest)
if __name__ == "__main__":
    Helper(__name__)
