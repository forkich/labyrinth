#!/usr/bin/env python3

from datetime import datetime

import asyncio
import signal
import aioxmpp
import aioxmpp.dispatcher
import collections

class AsyncDeque:
    def __init__(self):
        self.__data = collections.deque()
        self.__has_data = asyncio.Event()
        self.__has_data.clear()

    def put_nowait(self, obj):
        self.__data.append(obj)
        self.__has_data.set()

    def get_nowait(self):
        try:
            item = self.__data.popleft()
        except IndexError:
            raise asyncio.QueueEmpty() from None
        if not self.__data:
            self.__has_data.clear()
        return item

    @property
    def has_data(self):
        return self.__has_data.is_set()

    async def wait(self):
        await self.__has_data.wait()

class JabberProxy:

    __instance = None

    def __init__(self, **kwargs):
        self.__handlers = kwargs
        self.__client = None
        self.__group_chat = None
        self.__room = None
        self.__room_future = None
        self.__messages = AsyncDeque()
        self.__class__.__instance = self

    @classmethod
    def connect(cls, jid, password):
        proxy = cls.__instance

        proxy.__client = aioxmpp.PresenceManagedClient(
            aioxmpp.JID.fromstr(jid),
            aioxmpp.make_security_layer(
                password
            )
        )
        proxy.__group_chat = proxy.__client.summon(
            aioxmpp.MUCClient
        )

        dispatcher = proxy.__client.summon(
            aioxmpp.dispatcher.SimpleMessageDispatcher
        )
        dispatcher.register_callback(
            aioxmpp.MessageType.CHAT,
            None,
            proxy.__handlers.get("on_private", proxy.__on_private)
        )

    @classmethod
    def is_connected(cls):
        return cls.__instance.__client is not None

    @classmethod
    def join(cls, muc_jid, nick):
        proxy = cls.__instance

        proxy.__room, proxy.__room_future = proxy.__group_chat.join(
            aioxmpp.JID.fromstr(muc_jid),
            nick,
            history=aioxmpp.muc.xso.History(maxstanzas=0)
        )

        proxy.__room.on_message.connect(
            proxy.__handlers.get("on_message", proxy.__on_message)
        )
        proxy.__room.on_muc_enter.connect(
            proxy.__handlers.get("on_muc_enter", proxy.__on_muc_enter)
        )
        proxy.__room.on_exit.connect(
            proxy.__handlers.get("on_exit", proxy.__on_exit)
        )
        proxy.__room.on_join.connect(
            proxy.__handlers.get("on_join", proxy.__on_join)
        )
        proxy.__room.on_leave.connect(
            proxy.__handlers.get("on_leave", proxy.__on_leave)
        )

    @classmethod
    async def leave(cls):
        proxy = cls.__instance
        if not proxy.__room:
            return
        await proxy.__room.leave()
        proxy.__room = None

    @classmethod
    def send(cls, message, to=None):
        cls.__instance.__messages.put_nowait(
            (message, to)
        )

    @classmethod
    async def send_message(cls, msg_tuple):
        proxy = cls.__instance
        message, to = msg_tuple
        if to and proxy.__client:
            msg = aioxmpp.Message(
                type_=aioxmpp.MessageType.CHAT,
                to=aioxmpp.JID.fromstr(to)
            )
            msg.body[None] = message
            await proxy.__client.send(msg)
        elif not to and proxy.__room:
            msg = aioxmpp.Message(
                type_=aioxmpp.MessageType.GROUPCHAT,
            )
            msg.body[None] = message
            await proxy.__room.send_message(msg)

    def __on_private(self, message):
        body = message.body.lookup(
            [aioxmpp.structs.LanguageRange.fromstr("*")]
        )
        print("{} {}: {}".format(
            datetime.utcnow().isoformat(),
            str(message.from_),
            body
        ))

    def __on_message(self, message, member, source, **kwargs):
        body = message.body.lookup(
            [aioxmpp.structs.LanguageRange.fromstr("*")]
        )
        print("{} {}: {}".format(
            datetime.utcnow().isoformat(),
            member.nick,
            body
        ))

    def __on_muc_enter(self, presence, occupant, **kwargs):
        print("{} *** entered room {}".format(
            datetime.utcnow().isoformat(),
            presence.from_.bare()
        ))

    def __on_exit(self, **kwargs):
        print("{} *** left room".format(
            datetime.utcnow().isoformat()
        ))

    def __on_join(self, member, **kwargs):
        print("{} *** {} [{}] entered room".format(
            datetime.utcnow().isoformat(),
            member.nick,
            member.direct_jid
        ))

    def __on_leave(self, member, muc_leave_mode=None, **kwargs):
        print("{} *** {} [{}] left room ({})".format(
            datetime.utcnow().isoformat(),
            member.nick,
            member.direct_jid,
            muc_leave_mode
        ))

    @classmethod
    def members(cls):
        return cls.__instance.__room.members

    @classmethod
    async def run(cls):
        proxy = cls.__instance

        proxy.__stop_event = asyncio.Event()
        loop = asyncio.get_event_loop()
        loop.add_signal_handler(
            signal.SIGINT,
            proxy.__stop_event.set
        )

        while not proxy.__client:
            await asyncio.sleep(0.1)

        async with proxy.__client.connected():

            try:
                while not proxy.__stop_event.is_set():

                    if not proxy.__room:
                        await asyncio.sleep(0.1)
                        continue

                    done, pending = await asyncio.wait(
                        [
                            proxy.__room_future,
                            proxy.__messages.wait(),
                            proxy.__stop_event.wait()
                        ],
                        return_when=asyncio.FIRST_COMPLETED
                    )
                    if proxy.__room:
                        while proxy.__messages.has_data:
                            await proxy.send_message(
                                proxy.__messages.get_nowait()
                            )
                    if proxy.__room_future in done:
                        for future in pending:
                            future.cancel()

            finally:
                proxy.__room_future.cancel()

    @classmethod
    def stop(cls):
        cls.__instance.__stop_event.set()

