#!/usr/bin/env python3

from helper import *

import subprocess
from threading import Thread

def stdout_handler(stream):
    if not stream:
        return
    for line in stream:
        if not line:
            return
        print(line, end='')

def cli(commands):
    proc = subprocess.Popen(
            [
                "python3",
                "../cli/cli.py",
                "layout_01.txt",
                "rules.py",
                "bindings.py"
            ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf8",
            )

    Thread(target=stdout_handler, args=[proc.stdout]).start()

    for cmd in commands:
        proc.stdin.write(cmd + '\n')

    proc.communicate("quit\n")
    print()

def test_01_quit():
    cli([])

def test_02_help():
    cli(["help"])

def test_03_player():
    cli([
        "player p1 1 1",
        "start",
        "walk p1 e",
        "walk p1 s"
        ])

def test_04_more_players():
    cli([
        "player p1 1 1",
        "player p2 2 4",
        "player p3 3 4",
        "start",
        "walk p1 n"
        ])

if __name__ == "__main__":
    Helper(__name__)

