
import objects

import typing
import json
import inspect

class PlayerMeta(objects.ObjBaseMeta):

    def __init__(self, *args, **kwargs):
        self.__registry = {}

    def __getitem__(self, name):
        return self.__registry.get(name, None)

    @property
    def registry(self):
        return self.__registry

class Player(objects.ObjBase, metaclass=PlayerMeta):

    class Position(typing.NamedTuple):
        x: int = 0
        y: int = 0

        def __eq__(self, xy):
            return type(xy) == type(()) and self.x == xy[0] and self.y == xy[1]

    __MAX_HEALTH = 10

    class Inventory:

        __MAX_ITEMS = 10

        def __init__(self, player, items=None):
            if items is None:
                items = []
            self.__player = player
            self.__items = [items[i] if i < len(items) else None
                            for i in range(self.__MAX_ITEMS)]
            for item in self.__items:
                if item:
                    item.owner = player

        def __str__(self):
            return json.dumps(
                [str(item) if item else None for item in self.__items])

        def __len__(self):
            return self.__MAX_ITEMS

        @property
        def items(self):
            return self.__items

        def count(self, item):
            return list(map(str, self.__items)).count(str(item))

        def update(self):
            self.__player.update(inv=self.__str__())

        def push(self, item):
            for i in range(self.__MAX_ITEMS):
                if not self.__items[i]:
                    self.__items[i] = item
                    item.owner = self.__player
                    del item
                    self.update()
                    return True
            return False

        def __find_item(self, locator):
            if type(locator) == type(0) and 0 <= locator < self.__MAX_ITEMS:
                return locator
            elif type(locator) == type(""):
                for i in range(self.__MAX_ITEMS):
                    if self.__items[i].__class__.__name__ == locator:
                        return i
            elif inspect.isclass(locator):
                for i in range(self.__MAX_ITEMS):
                    if isinstance(self.__items[i], locator):
                        return i
            return None

        def __getitem__(self, locator):
            index = self.__find_item(locator)
            if index is not None:
                return self.__items[index]
            raise IndexError("Invalid index '{locator}'")

        def pull(self, locator):
            index = self.__find_item(locator)
            if index is not None:
                item = self.__items[index]
                if not hasattr(item, "is_reusable") or not item.is_reusable:
                    self.__items[index] = None
                self.update()
                return item
            return None

    def __init__(self, name, x, y, items=None, **kwargs):
        self.__name = name
        self.__pos = Player.Position()
        self.__inv = Player.Inventory(self, items if items else [])
        self.__health = self.__MAX_HEALTH
        self.update(health=self.__health,
                    inv=str(self.__inv))
        self.move_abs(x, y)  # force "position update" notification
        for k, v in kwargs.items():
            self.__dict__[k] = v
        self.__class__.registry[name] = self
#        ObjBase.update(self, "players",
#                       players=list(Player.registry.keys()))

    def update(self, **kwargs):
        objects.ObjBase.update(self, "status",
                               name=self.__name,
                               **kwargs)

    @property
    def name(self):
        return self.__name

    @property
    def pos(self):
        return self.__pos

    @property
    def inv(self):
        return self.__inv

    @property
    def health(self):
        return self.__health

    @health.setter
    def health(self, value):
        self.__health = min(value, self.__MAX_HEALTH)
        self.update(health=self.__health)

    def move_abs(self, x, y):
        self.__pos = Player.Position(x, y)
        objects.ObjBase.update(self, "position",
                               name=self.__name,
                               x=self.__pos.x,
                               y=self.__pos.y)

    def move_rel(self, inc):
        if type(inc) == type(()) and len(inc) == 2:
            self.move_abs(self.__pos.x + inc[0], self.__pos.y + inc[1])
        else:
            raise ValueError(f"Expected (x, y) got '{inc}'")
