import json
import shlex
import sys

import pubsub.pub

sys.path.append("../Server")
from commands import Commands
from dispatcher import Dispatcher
from player import Player

class Config(object):
    __instance = None

    def __init__(self, labyrinth=None):
        self.__labyrinth = labyrinth
        self.__cmd_subscriber = None
        if labyrinth is not None:
            self.__cmd_subscriber = Commands(labyrinth)
        self.__cmd_publisher = Commands()
        self.__dispatcher = Dispatcher()
        self.__players = {}
        self.__player_inventories = {}
        self.__class__.__instance = self
        pubsub.pub.subscribe(self.__remote_add_player, "Player.color")
        pubsub.pub.subscribe(self.__remote_set_player_inventory, "Player.status")

    @classmethod
    def get_instance(cls):
        assert cls.__instance is not None
        return cls.__instance

    def add_player(self, name, color):
        if self.__labyrinth:
            self.__players[name] = color
            pubsub.pub.sendMessage("Player.color", name=name, color=color)

    def __remote_add_player(self, topicObj=pubsub.pub.AUTO_TOPIC, name="",
                            color="#ff0000", **kwargs):
        self.__players[name] = color

    def __remote_set_player_inventory(self, topicObj=pubsub.pub.AUTO_TOPIC,
                                      name=None, health=None, inv=None, **kwargs):
        if inv is not None:
            self.__player_inventories[name] = json.loads(inv)

    def has_player(self, name):
        return name in self.__players

    @property
    def labyrinth_size(self):
        return self.__labyrinth.size_x, self.__labyrinth.size_y

    @property
    def labyrinth_layout(self):
        if self.__labyrinth:
            return self.__labyrinth.map
        else:
            return ""

    @property
    def commands(self):
        return self.__cmd_publisher

    @property
    def players(self):
        return self.__players

    def get_inventory_items(self, player_name):
        if self.__labyrinth is not None:
            return Player[player_name].inv.items
        else:
            return self.__player_inventories[player_name]

    @property
    def inventory_size(self):
        return len(self.__labyrinth.inv)

    def get_health(self, player):
        return Player[player].health

    def get_inventory(self, player):
        return Player[player].inv

    @property
    def inventory_size(self):
        return len(self.__labyrinth.inv)

    def get_health(self, player):
        return Player[player].health

    @classmethod
    def execute_command(cls, command):
        cmd, *cmd_args = shlex.split(command)
        if cmd == "player":
            cls.__instance.__players[cmd_args[0]] = "#FFFFFF"
            cls.__instance.commands.player(*cmd_args)
        if cmd == "use":
            cls.__instance.commands.use(*cmd_args)
        if cmd == "start":
            cls.__instance.commands.start(*cmd_args)
        if cmd == "walk":
            cls.__instance.commands.walk(*cmd_args)

