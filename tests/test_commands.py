#!/usr/bin/env python3

from helper import *

import sys

sys.path.append("../Server")

from pubsub import pub

from labyrinth import Labyrinth
from commands import Commands
from player import Player


# snoop pubsub messages, print if topic is not in suppress list

def snoop(topicObj=pub.AUTO_TOPIC, **kwargs):
    suppress_list = [
        "Labyrinth.map"
    ]
    topic = topicObj.getName()
    if topic not in suppress_list:
        print(f"{topic:<20} {kwargs}")


pub.subscribe(snoop, pub.ALL_TOPICS)


def layout_from_file(path):
    with open(path) as f:
        layout = f.read()
    return layout.rstrip()


def exec_cmd(command, **kwargs):
    topic = "Command." + command
    pub.sendMessage(topic, **kwargs)


def test_01_add_player_and_order():
    l = Labyrinth(layout_from_file('layout_01.txt'))
    cmd = Commands(l)
    cmd.player("player_1", 1, 1)

    exec_cmd("player", name="player_2", x=2, y=2)

    pub_cmd = Commands()
    pub_cmd.player("player_3", 3, 3)

    assert Player["player_1"].pos == (1, 1)
    assert Player["player_2"].pos == (2, 2)
    assert Player["player_3"].pos == (3, 3)


if __name__ == "__main__":
    Helper(__name__)
