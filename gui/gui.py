#!/usr/bin/env python3
import tkinter as tk
import sys
import asyncio
import getpass
import aioxmpp

from pubsub import pub
from main_window import MainWindow
from config import Config
import argparse
sys.path.append("..")
from xml_rpc.jabber_xmlrpc_manager import JabberXMLRPCManager
from Server.labyrinth import Labyrinth
from Server.player import Player
from Server.objects import ObjBase
from collections import deque

def send_message(topic, kwargs_dict):
    topic += ".remote"
    pub.sendMessage(topic, **kwargs_dict)

message_deque = deque()

def listener(topicObj=pub.AUTO_TOPIC, **kwargs):
    global message_deque
    print("In process message!")
    topic = topicObj.getName()
    if not topic.endswith(".remote"):
        message_deque.append((topic, kwargs))
    else:
        print("processed remote message, not sending")

async def run_manager(jid, password, to):
    global message_deque
    manager = JabberXMLRPCManager(allow_none=True)
    manager.connect(jid, password, to)
    manager.register_introspection_functions()
    manager.register_function(send_message)

    pub.subscribe(listener, pub.ALL_TOPICS)
    try:
        async with manager.connected():
            while True:
                if len(message_deque) != 0:
                    await manager.send_message(*(message_deque.popleft()))
                await asyncio.sleep(0.01)
    except TimeoutError as e:
        print(f"Timeout in run_manager: {e}")

async def run_tk(root):
    exit_flag = False

    def on_exit():
        nonlocal exit_flag
        exit_flag = True
        root.destroy()

    root.protocol("WM_DELETE_WINDOW", on_exit)
    while not exit_flag:
        await asyncio.sleep(0.01)
        root.update()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("role", choices=("admin", "player"))
    for arg in ("jid", "to", "password", "layout", "rules", "bindings"):
        parser.add_argument("-" + arg[0], "--" + arg)
    args = parser.parse_args()

    jid = aioxmpp.JID.fromstr(args.jid)
    to = aioxmpp.JID.fromstr(args.to)

    if args.password:
        password = args.password
    else:
        password = getpass.getpass()

    layout = rules = map_bindings = None
    labyrinth = None
    prerequisites = False
    if args.role == "admin":
        prerequisites = all((args.layout, args.bindings, args.rules))
        if prerequisites:
            with open(args.layout) as file:
                layout = file.read().rstrip()
            with open(args.rules) as file:
                rules = file.read()
            with open(args.bindings) as file:
                bindings = file.read()
            exec(compile(rules, args.rules, 'exec'))
            map_bindings = eval(compile(bindings, args.bindings, 'eval'))
            labyrinth = Labyrinth(layout, map_bindings)
        else:
            parser.error("Admin mode requires all of "
                         "layout, rules and bindings arguments")
    elif args.role == "player":
        prerequisites = not any((args.layout, args.bindings, args.rules))
        if not prerequisites:
            parser.error("Player mode prohibits any of "
                         "layout, rules and bindings arguments")

    root = tk.Tk()
    root.title(f"{args.jid} ({args.role})")
    config = Config(labyrinth)
    main_window = MainWindow(root)
    main_window.pack(fill=tk.BOTH, expand="yes")

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(asyncio.gather(
            run_manager(jid, password, to),
            run_tk(root)))
    except KeyboardInterrupt:
        print(f"Keyboard interrupt in the main script (role = {args.role})")

    loop.close()
