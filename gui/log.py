#
# log.py
#

import tkinter as tk


class Log(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        self.__contents = tk.Text(self, height=30, width=50)
        self.__contents_scrollbar = tk.Scrollbar(self, orient="vertical",
                                                 command=self.__contents.yview)
        self.__contents['yscrollcommand'] = self.__contents_scrollbar.set
        self.__contents['state'] = 'disabled'

        self.__contents.grid(column=0, row=0, sticky="nswe")
        self.__contents_scrollbar.grid(column=1, row=0, sticky="ns")

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

    def display_message(self, contents):
        # TODO time, topic
        self.__contents['state'] = 'normal'
        self.__contents.insert("end", str(contents) + '\n')
        self.__contents.see(tk.END)
        self.__contents['state'] = 'disabled'
